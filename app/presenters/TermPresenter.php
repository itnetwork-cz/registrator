<?php

namespace App\Presenters;

use App\Model\TermManager;

/**
 * Třída TermPresenter.
 * @package App\Presenters
 */
class TermPresenter extends BasePresenter
{
	/** @var TermManager Model pro práci s termíny. */
	private $termManager;

	/**
	 * Konstruktor třídy TermPresenter.
	 * @param TermManager $termManager model pro práci s termíny
	 */
	public function __construct(TermManager $termManager)
	{
		parent::__construct();
		$this->termManager = $termManager;
	}

	/** Akce pro vykreslení všech termínů. */
	public function renderDefault()
	{
		$this->template->terms = $this->termManager->getAllTerms();
	}
}
