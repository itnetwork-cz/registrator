<?php

namespace App\Model;

use Nette\Database\UniqueConstraintViolationException;
use Nette\Model\CRUDManager;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\Passwords;

/**
 * Class MemberManager
 * @package App\Model
 */
class MemberManager extends CRUDManager implements IAuthenticator
{
	/** Konstanty pro práci s modelem. */
	const
		COLUMN_ID = 'member_id',
		COLUMN_EMAIL = 'email',
		COLUMN_PASSWORD_HASH = 'password',
		COLUMN_ROLE = 'role';

	/**
	 * @inheritdoc
	 */
	public function authenticate(array $credentials)
	{
		list($email, $password) = $credentials;

		$row = $this->getTable()->where(self::COLUMN_EMAIL, $email)->fetch();

		if (!$row)
			throw new AuthenticationException('Zadali jste neexistující email.', self::IDENTITY_NOT_FOUND);

		elseif (!Passwords::verify($password, $row[self::COLUMN_PASSWORD_HASH]))
			throw new AuthenticationException('Vámi zadané heslo není správně.', self::INVALID_CREDENTIAL);

		elseif (Passwords::needsRehash($row[self::COLUMN_PASSWORD_HASH]))
			$row->update([self::COLUMN_PASSWORD_HASH => Passwords::hash($password)]);

		$arr = $row->toArray();
		unset($arr[self::COLUMN_PASSWORD_HASH]);
		return new Identity($row[self::COLUMN_ID], $row[self::COLUMN_ROLE], $arr);
	}

	/**
	 * Registruje nového uživatele.
	 * @param string $email
	 * @param string $password
	 * @throws UniqueConstraintViolationException Jestliže uživatel s tímto emailem již existuje.
	 */
	public function register($email, $password)
	{
		$this->getTable()->insert(array(
			self::COLUMN_EMAIL => $email,
			self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
		));
	}
}
