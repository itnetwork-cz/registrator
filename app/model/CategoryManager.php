<?php

namespace App\Model;

use Nette\Model\CRUDManager;
use Nette\Utils\ArrayHash;

/**
 * Class CategoryManager
 * @package App\Model
 */
class CategoryManager extends CRUDManager
{
	/** Konstanty pro práci s modelem. */
	const
		COLUMN_ID = 'category_id',
		COLUMN_TITLE = 'title';

	/**
	 *
	 * @return ArrayHash
	 */
	public function getCategoryPairs()
	{
		return ArrayHash::from($this->getTable()->fetchPairs(self::COLUMN_ID, self::COLUMN_TITLE));
	}
}
