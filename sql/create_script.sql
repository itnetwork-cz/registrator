CREATE TABLE address
(
  address_id INTEGER PRIMARY KEY NOT NULL,
  street TEXT NOT NULL,
  house_number INTEGER NOT NULL,
  city TEXT NOT NULL,
  reg_number INTEGER,
  zip_code INTEGER
);

CREATE TABLE category
(
  category_id INTEGER PRIMARY KEY NOT NULL,
  title TEXT NOT NULL
);

CREATE TABLE category_course
(
  category_id INTEGER NOT NULL,
  course_id INTEGER NOT NULL,
  CONSTRAINT category_course_pkey PRIMARY KEY (category_id, course_id)
);

CREATE TABLE company
(
  company_id INTEGER PRIMARY KEY NOT NULL,
  ident_number TEXT NOT NULL,
  tax_number TEXT
);

CREATE TABLE course
(
  course_id INTEGER PRIMARY KEY NOT NULL,
  title TEXT NOT NULL,
  url TEXT NOT NULL,
  price NUMERIC NOT NULL,
  duration NUMERIC NOT NULL,
  description TEXT NOT NULL
);

CREATE TABLE member
(
  member_id INTEGER PRIMARY KEY NOT NULL,
  email TEXT NOT NULL,
  password VARCHAR(60) NOT NULL,
  name TEXT NOT NULL,
  surname TEXT NOT NULL,
  role ROLE NOT NULL,
  company_id INTEGER
);

CREATE TABLE term
(
  term_id INTEGER PRIMARY KEY NOT NULL,
  start TIMESTAMP NOT NULL,
  "end" TIMESTAMP NOT NULL,
  max_capacity INTEGER NOT NULL,
  min_capacity INTEGER DEFAULT 0,
  room TEXT,
  address_id INTEGER NOT NULL
);

CREATE TABLE term_member
(
  member_id INTEGER NOT NULL,
  term_id INTEGER NOT NULL,
  lector BOOLEAN DEFAULT false,
  CONSTRAINT term_member_pkey PRIMARY KEY (member_id, term_id)
);

CREATE UNIQUE INDEX category_title_uindex ON category (title);
ALTER TABLE category_course ADD FOREIGN KEY (category_id) REFERENCES category (category_id);
ALTER TABLE category_course ADD FOREIGN KEY (course_id) REFERENCES course (course_id);
CREATE UNIQUE INDEX course_title_uindex ON course (title);
CREATE UNIQUE INDEX course_url_uindex ON course (url);
ALTER TABLE member ADD FOREIGN KEY (company_id) REFERENCES company (company_id);
CREATE UNIQUE INDEX member_email_uindex ON member (email);
ALTER TABLE term ADD FOREIGN KEY (address_id) REFERENCES address (address_id);
ALTER TABLE term_member ADD FOREIGN KEY (member_id) REFERENCES member (member_id);
ALTER TABLE term_member ADD FOREIGN KEY (term_id) REFERENCES term (term_id);
