# Registrator

Projekt Registrátor ze srazu ITnetwork.

## Diagramy

### [Use Case Diagram](https://drive.google.com/file/d/0B4DZtHFmq6J1dFc5M3prMGFjSXM/view?usp=sharing)

### [Database Model](https://drive.google.com/file/d/0B4DZtHFmq6J1dDM4a0dCbGJUU0U/view?usp=sharing)

### [Class Diagram](https://drive.google.com/file/d/0B4DZtHFmq6J1d0JQSXdpVmg5QmM/view?usp=sharing)
